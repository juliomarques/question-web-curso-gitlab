FROM trion/ng-cli as base

WORKDIR /tmp

COPY package.json .

RUN npm install

COPY . .

RUN ng build

COPY --from=base /tmp/dist/question-web /usr/share/nginx/html
